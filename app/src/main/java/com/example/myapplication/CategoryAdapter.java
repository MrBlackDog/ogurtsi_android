package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder>{

    private final LayoutInflater inflater;
    private final List<Building> buildings;
    boolean[] checked;

    CategoryAdapter(Context context, List<Building> buildings) {
        this.buildings = buildings;
        this.inflater = LayoutInflater.from(context);
        checked = new boolean[buildings.size()];
    }

    @NonNull
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {
        Building building = buildings.get(position);
        holder.nameView.setText(building.getName());
        holder.categoryView.setText(building.getCategory());
        holder.descriptionView.setText(building.getDescription());
        holder.checkBox.setChecked(checked[position]);
        holder.checkBox.setOnClickListener(v -> checked[position] = !checked[position]);
    }

    public boolean[] getChecked() {
        return checked;
    }

    @Override
    public int getItemCount() {
        return buildings.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView nameView,categoryView,descriptionView;
        CheckBox checkBox;
        ViewHolder(View view){
            super(view);
            checkBox = view.findViewById(R.id.checkBox);
            nameView = (TextView) view.findViewById(R.id.textName);
            categoryView = (TextView) view.findViewById(R.id.textCategory);
            descriptionView = (TextView) view.findViewById(R.id.textDescription);
        }
    }

}
