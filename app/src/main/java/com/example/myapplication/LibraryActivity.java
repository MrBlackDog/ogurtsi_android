package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LibraryActivity extends AppCompatActivity {

    ArrayList<Building> buildingArrayList = new ArrayList<Building>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);
        Button button = findViewById(R.id.button);
        //        GetBuildings();
        ManualInit();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        // создаем адаптер
        CategoryAdapter adapter = new CategoryAdapter(this, buildingArrayList);
        // устанавливаем для списка адаптер
        recyclerView.setAdapter(adapter);
        // код в активити для получения чекнутых значений.
        boolean[] checked = adapter.getChecked();
        button.setOnClickListener(v -> {
            Intent intent = new Intent(LibraryActivity.this, MainActivity.class);
            intent.putExtra("checked",checked);
            intent.putExtra("buildings",buildingArrayList);
            startActivity(intent);
        });
    }

    private void ManualInit() {
        buildingArrayList.add(new Building("Главный корпус","University","Главный корпус НИУ МЭИ",55.75479779307307, 37.70804199780322));
        buildingArrayList.add(new Building("Корпус И","University","Корпус \"И\" НИУ МЭИ",55.75387404477949, 37.7081814726624));
        buildingArrayList.add(new Building("Корпус М","University","Корпус \"М\" НИУ МЭИ",55.75630047568958, 37.70456892965809));

//        buildingArrayList.add(new Building("Main","University","Main Building of MPEI",55.75479779307307, 37.70804199780322));
//        buildingArrayList.add(new Building("I","University","Building I of MPEI",55.75387404477949, 37.7081814726624));
//        buildingArrayList.add(new Building("M","University","Building M of MPEI",55.75630047568958, 37.70456892965809));
    }

    public void GetBuildings(){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://mrblackdog.ddns.net:6000/home/Table")
                .build();
        //client.setConnectTimeout(15, TimeUnit.SECONDS);

        client.newCall(request).enqueue(new Callback() {
            @Override public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                Log.d("ArTack",response.body().toString());
            }
        });
    }
}