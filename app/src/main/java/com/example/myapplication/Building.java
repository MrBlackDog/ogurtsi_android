package com.example.myapplication;

import java.io.Serializable;

public class Building implements Serializable {
    private String name;
    private String category;
    private String description;
    private double latitude;
    private double longitude;

    public Building(String name, String category, String description, double latitude, double longitude) {
        this.name = name;
        this.category = category;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
